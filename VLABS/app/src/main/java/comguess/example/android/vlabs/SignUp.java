package comguess.example.android.vlabs;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SignUp extends AppCompatActivity {
    Button submit;
    EditText name,password;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String Name = "nameKey";
    public static final String Password = "passwordKey";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user__sign_up);
        name = (EditText)findViewById(R.id.name);
        password = (EditText)findViewById(R.id.password);
        submit = (Button)findViewById(R.id.submit);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String  str=name.getText().toString();
                String  pass = password.getText().toString();
                sharedpreferences = getSharedPreferences(mypreference,
                        Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(Name,str);
                editor.putString(Password, pass);
                editor.commit();
                if(str.equalsIgnoreCase(""))
                {
                    name.setError("please enter username");//it gives user to info message //use any one //
                }
                if(pass.length()<8){
                    password.setError("enter minimum of 8 digits");
                }
                else if(str.length()>3 && pass.length()>8){
                    Intent loginpage = new Intent(view.getContext(),HomePage.class);
                    startActivity(loginpage);
                }

            }
        });
    }
}

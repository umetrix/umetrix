package comguess.example.android.vlabs;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

public class Undo extends AppCompatActivity {
    ImageButton undo,del;
    EditText text;
    String temp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_undo);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        undo = (ImageButton)findViewById(R.id.undo);
        del = (ImageButton)findViewById(R.id.del);
        text = (EditText)findViewById(R.id.text);
        temp = "";
    }
    public void undoOperation(View v) {
        if(v.getId()==R.id.undo){
            text.setText(temp);
        }else if(v.getId()==R.id.del){

            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Warning")
                    .setMessage("Do you Want delete the data")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            temp = text.getText().toString();
                            text.setText("");
                        }

                    })
                    .setNegativeButton("No", null)
                    .show();
        }

        }

}